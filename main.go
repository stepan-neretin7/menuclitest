package main

import (
	"bufio"
	"client/menu"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	mainMenu := menu.Menu{Commands: []menu.Command{
		{"help", func(s menu.State, m menu.Menu) error {
			fmt.Println("Hello world!")
			return nil
		}},
		{
			"login", func(s menu.State, m menu.Menu) error {
				var password string
				scanner := bufio.NewReader(os.Stdin)
				password, _ = scanner.ReadString('\n')
				if strings.TrimSuffix(password, "\n") != "123"{
					return errors.New("wrong password")
				}
				fmt.Println("Успешная авторизация")
				s.SetMenu(m)
				return nil
			},
		},
	}}
	loggedMenu := menu.Menu{Commands: []menu.Command{
		{"test", func(s menu.State, m menu.Menu) error {
			fmt.Println("Hello, user")
		}},
	}}
	state := menu.NewState(mainMenu)

	for {
		m:= state.GetMenu()
		fmt.Println(m.Commands)
		time.Sleep(2*time.Second)
	}
}
