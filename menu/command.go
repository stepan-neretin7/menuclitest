package menu

// Command struct allows you control your command
type action func(s State, m Menu) error

type Command struct {
	Name string
	Handler action
}
